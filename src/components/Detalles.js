import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import "./Detalles.less";

class Detalles extends Component {
	constructor(props) {
		console.log(props);
		super();
		this.state = {
			pelicula: props.location.state.data.pelicula,
			genero: props.location.state.data.genero,
			fireRedirect: false,
			dominio: "http://localhost:8080/",
		};
	}

	eliminarPelicula = () => {
		let options = {
			method: "DELETE",
			headers: {
				Authentication: "CastroCarazo",
			},
		};

		fetch(this.state.dominio + "pelicula/" + this.state.pelicula.id, options)
			.then((response) => {
				if (response.ok) {
					console.log("Exitoso");
				}
			})
			.then(() => {
				this.setState({ fireRedirect: true });
			})
			.catch((error) => {
				console.log("Fallo: " + error.message);
			});
	};

	render() {
		const { pelicula, genero, fireRedirect } = this.state;
		const { from } = this.props.location.state || "/";

		// console.log(genero);

		return (
			<div className="container-fluid">
				<div className="row">
					<div className="container">
						<div className="row nav">
							<Link
								to={{
									pathname: `/actualizar`,
									state: { pelicula: pelicula },
								}}
							>
								Actualizar
							</Link>
							<button onClick={this.eliminarPelicula}>Eliminar</button>
						</div>
						<div className="row detalles">
							<div className="col-12 col-lg-5">
								<img src={pelicula.imagenHd} alt={pelicula.nombre} />
							</div>

							<div className="col-12 col-lg-6 offset-lg-1">
								<h1>{pelicula.nombre}</h1>
								<h3>Año de Lanzamiento</h3>
								<span>{pelicula.anioLanzamiento}</span>
								<h3>Género</h3>
								<span>{genero.nombre}</span>
								<h3>Sinopsis</h3>
								<p>{pelicula.descripcion}</p>
							</div>
						</div>
					</div>
				</div>

				{fireRedirect && <Redirect to={from || "/"} />}
			</div>
		);
	}
}

export default Detalles;
