import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Pelicula.less";

class Pelicula extends Component {
	constructor() {
		super();
		this.state = {
			peliculas: [],
			generos: [],
			dominio: "http://localhost:8080/",
		};
	}

	componentDidMount() {
		this.getData("pelicula");
		this.getData("genero");
	}

	getData = (endpoint) => {
		let options = {
			method: "GET",
			headers: {
				Authentication: "CastroCarazo",
			},
		};

		fetch(this.state.dominio + endpoint, options)
			.then((response) => response.json())
			.then((data) => {
				if (endpoint === "pelicula") {
					this.setState({ peliculas: data });
					// console.log(data);
				} else {
					this.setState({ generos: data });
					// console.log(data);
				}
			});
	};

	truncateText = (text) => {
		let length = 125;

		if (text.length > length) {
			return text.substring(0, text.lastIndexOf(" ", length)) + "...";
		} else {
			return text;
		}
	};

	convertToSlug = (text) => {
		return text
			.toLowerCase()
			.replace(/ /g, "-")
			.replace(/[^\w-]+/g, "");
	};

	render() {
		const { peliculas, generos } = this.state;

		return (
			<div className="container">
				<div className="nav row">
					<Link
						to={{
							pathname: `/agregar`,
						}}
					>
						Agregar
					</Link>
				</div>

				<div className="lista row">
					{peliculas.map((pelicula) => (
						<div
							key={pelicula.id}
							className="pelicula col-12 col-md-4 col-xl-4"
						>
							<div className="content">
								<img src={pelicula.imagenSd} alt={pelicula.nombre} />

								<div className="info">
									<h1>{pelicula.nombre}</h1>
									<p>{this.truncateText(pelicula.descripcion)}</p>
									<Link
										to={{
											pathname: `/${this.convertToSlug(pelicula.nombre)}`,
											state: {
												data: {
													pelicula: pelicula,
													genero: generos[pelicula.generoId - 1],
												},
											},
										}}
									>
										Leer más
									</Link>
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		);
	}
}

export default Pelicula;
