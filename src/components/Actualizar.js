import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import "./Actualizar.less";

class Actualizar extends Component {
	constructor(props) {
		super();
		this.state = {
			pelicula: props.location.state.pelicula,
			generos: [],
			generoId: props.location.state.pelicula.generoId,
			fireRedirect: false,
			dominio: "http://localhost:8080/",
		};
	}

	componentDidMount() {
		this.getData();
	}

	getData = () => {
		let options = {
			method: "GET",
			headers: {
				Authentication: "CastroCarazo",
			},
		};

		fetch(this.state.dominio + "genero", options)
			.then((response) => response.json())
			.then((data) => {
				this.setState({ generos: data });
				// console.log(data);
			});
	};

	buildURLQuery = (obj) =>
		Object.entries(obj)
			.map((pair) => pair.map(encodeURIComponent).join("="))
			.join("&");

	submitData = (event) => {
		event.preventDefault();

		let pelicula = {
			nombre: event.target[0].value,
			anioLanzamiento: event.target[1].value,
			generoId: event.target[2].value,
			imagenSd: event.target[3].value,
			imagenHd: event.target[4].value,
			descripcion: event.target[5].value,
		};

		let options = {
			method: "PUT",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authentication: "CastroCarazo",
			},
		};

		fetch(
			this.state.dominio +
				"pelicula/" +
				this.state.pelicula.id +
				"?" +
				this.buildURLQuery(pelicula),
			options
		)
			.then((response) => {
				if (response.ok) {
					console.log("Exitoso");
				}
			})
			.then(() => {
				this.setState({ fireRedirect: true });
			})
			.catch((error) => {
				console.log("Fallo:" + error.message);
			});
	};

	handleChange = (event) => {
		this.setState({ generoId: event.target.value });
	};

	render() {
		const { generos, pelicula, fireRedirect } = this.state;
		const { from } = this.props.location.state || "/";

		return (
			<div className="container">
				<div className="row formulario">
					<form onSubmit={this.submitData}>
						<label htmlFor="nombre">Nombre</label>
						<input
							type="text"
							name="nombre"
							defaultValue={pelicula.nombre}
							required
						/>

						<label htmlFor="anioLanzamiento">Año de Lanzamiento</label>
						<input
							type="number"
							name="anioLanzamiento"
							defaultValue={pelicula.anioLanzamiento}
							required
						/>

						<label htmlFor="generoId">Género</label>
						<select
							name="generoId"
							id="generoId"
							value={this.state.generoId}
							onChange={this.handleChange}
						>
							{generos.map((genero) => (
								<option key={genero.id} value={genero.id}>
									{genero.nombre}
								</option>
							))}
						</select>

						<label htmlFor="imagenSd">Imagen pequeña</label>
						<input
							type="text"
							name="imagenSd"
							defaultValue={pelicula.imagenSd}
							required
						/>

						<label htmlFor="imagenHd">Imagen grande</label>
						<input
							type="text"
							name="imagenHd"
							defaultValue={pelicula.imagenHd}
							required
						/>

						<label htmlFor="descripcion">Descripción</label>
						<textarea
							type="text"
							name="descripcion"
							defaultValue={pelicula.descripcion}
							required
						/>

						<input type="submit" value="Enviar" />

						{fireRedirect && <Redirect to={from || "/"} />}
					</form>
				</div>
			</div>
		);
	}
}

export default Actualizar;
