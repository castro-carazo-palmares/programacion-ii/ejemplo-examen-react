import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Pelicula from "./components/Pelicula";
import Detalles from "./components/Detalles";
import Actualizar from "./components/Actualizar";
import Agregar from "./components/Agregar";

function App() {
	return (
		<div className="App">
			<BrowserRouter>
				<Switch>
					<Route path="/agregar" component={Agregar} />
					<Route path="/actualizar" component={Actualizar} />
					<Route path="/:slug" component={Detalles} />
					<Route exact path="/" component={Pelicula} />
					<Redirect to="/" />
				</Switch>
			</BrowserRouter>
		</div>
	);
}

export default App;
